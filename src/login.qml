import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Extras 1.4

Window {
    id: loginWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Compte: rajout")
    color: "#e6ebf0"

    Image {
        id: image
        y: 123
        width: 185
        height: 234
        anchors.left: parent.left
        anchors.leftMargin: 71
        transformOrigin: Item.Left
        fillMode: Image.PreserveAspectFit
        source: "elephant.img"
    }

    Text {
        id: loginTitle
        x: 71
        text: qsTr("Ajout d'un compte")
        font.pixelSize: 25
        anchors.top: parent.top
        anchors.topMargin: 71
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 392
        transformOrigin: Item.Top
    }

    TextField {
        id: serverTextField
        x: 332
        y: 151
        width: 271
        height: 32
        placeholderText: qsTr("https://fediverse.example.com")
        renderType: Text.QtRendering
    }

    Text {
        id: serverText
        x: 332
        y: 123
        text: qsTr("Instance Mastodon ou Pleroma")
        font.pixelSize: 15
    }

    TextField {
        id: accountTextField
        x: 332
        y: 217
        width: 271
        height: 32
        placeholderText: qsTr("gargron")
        renderType: Text.QtRendering
    }

    Text {
        id: accountText
        x: 332
        y: 189
        text: qsTr("Compte")
        font.pixelSize: 15
    }

    TextField {
        id: passwordTextField
        x: 332
        y: 283
        width: 271
        height: 32
        text: qsTr("")
        renderType: Text.QtRendering
        echoMode: TextInput.Password
    }

    Text {
        id: passwordText
        x: 332
        y: 255
        text: qsTr("Mot de passe")
        font.pixelSize: 15
    }

    Button {
        id: cancelButton
        x: 21
        y: 427
        text: qsTr("Annuler")
        transformOrigin: Item.BottomLeft
    }

    Button {
        id: nextButton
        x: 532
        y: 427
        text: qsTr("Suivant")
        highlighted: true
        transformOrigin: Item.BottomLeft
    }

    Text {
        id: linkText
        x: 71
        y: 363
        color: "#0067b8"
        text: qsTr("Pas de compte ? Trouvez une instance adaptée !")
        font.underline: true
        fontSizeMode: Text.Fit
        font.pixelSize: 12
    }
}


