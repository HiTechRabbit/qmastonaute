import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Window {
    id: tootWindow
    width: 480
    height: 320
    title: "Nouveau pouet"
    color: "#e6ebf0"

    TextArea {
        id: textArea
        x: 7
        y: 7
        width: 466
        height: 262
        text: qsTr("")
        placeholderText: "Quoi d'neuf docteur?"

    }

    Button {
        id: tootButton
        x: 389
        y: 275
        text: qsTr("Pouet!")
    }
    
    Button {
        id: attachmentButton
        x: 7
        y: 275
        width: 34
        text: qsTr("")
        flat: false
        display: AbstractButton.IconOnly
        icon.source: "/icons/attachment"
    }
    
    Button {
        id: surveyButton
        x: 47
        y: 275
        width: 34
        text: qsTr("")
        flat: false
        display: AbstractButton.IconOnly
        icon.source: "/icons/list"
    }
    
    Button {
        id: privacyLevelButton
        x: 87
        y: 275
        width: 34
        text: qsTr("")
        flat: false
        display: AbstractButton.IconOnly
        icon.source: "/icons/globe"

        onClicked: privacyLevelMenu.open()

        state: "public"
        states: [
            State {
                name: "public"
                PropertyChanges {
                    target: privacyLevelButton;
                    icon.source: "/icons/globe"
                }
            },
            State {
                name: "unlisted"
                PropertyChanges {
                    target: privacyLevelButton;
                    icon.source: "/icons/unlock"
                }
            },
            State {
                name: "followers-only"
                PropertyChanges {
                    target: privacyLevelButton;
                    icon.source: "/icons/lock"
                }
            },
            State {
                name: "private"
                PropertyChanges {
                    target: privacyLevelButton;
                    icon.source: "/icons/message"
                }
            }
        ]
    }

    Menu {
        id: privacyLevelMenu
        y: 1000
        x: privacyLevelButton.x

        MenuItem {
            text: qsTr("Public")
            icon.source: "/icons/globe"
            onClicked: {
                privacyLevelButton.state = "public"
            }
        }
        MenuItem {
            text: qsTr("Non listé")
            icon.source: "/icons/unlock"
            onClicked: {
                privacyLevelButton.state = "unlisted"
            }
        }
        MenuItem {
            text: qsTr("Abonnés seulement")
            icon.source: "/icons/lock"
            onClicked: {
                privacyLevelButton.state = "followers-only"
            }
        }
        MenuItem {
            text: qsTr("Privé")
            icon.source: "/icons/message"
            onClicked: {
                privacyLevelButton.state = "private"
            }
        }
    }
    
    Button {
        id: contentWarninButton
        x: 127
        y: 275
        width: 34
        text: qsTr("CW")
        font.weight: Font.ExtraBold
        font.bold: false
        checked: false
        checkable: true
        flat: false
        display: AbstractButton.TextBesideIcon
        icon.source: "/icons/alert"
    }

}
