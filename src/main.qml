import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.12

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 1024
    height: 768
    title: qsTr("Mastodon | @adidal@chat.cdstm.ch")
    color: "#e6ebf0"

    Row {
        id: iconsRow
        height: 64
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 1024
        anchors.top: parent.top
        anchors.topMargin: 0
        Layout.fillWidth: true
    }

    Row {
        id: mainRow
        anchors.top: iconsRow.bottom
        anchors.topMargin: 0
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        Layout.fillWidth: true

        ScrollView {
            id: mainScrollView
            anchors.fill: parent
            ScrollBar.horizontal.policy: ScrollBar.AlwaysOn
            ScrollBar.vertical.policy: ScrollBar.AlwaysOff

            contentWidth: contentChildren.length * 480

            ScrollView {
                id: notificationsScrollView
                width: 480
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0

                ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ScrollBar.vertical.policy: ScrollBar.AlwaysOn

                Layout.fillHeight: true
                //anchors.left: parent

                Rectangle {
                    color: "red"
                    anchors.fill: parent
                }
            }
            ScrollView {
                id: homeScrollView
                width: 480
                anchors.left: notificationsScrollView.right
                anchors.leftMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0

                ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ScrollBar.vertical.policy: ScrollBar.AlwaysOn

                Layout.fillHeight: true
                //anchors.left: parent

                Rectangle {
                    color: "green"
                    anchors.fill: parent
                }
            }
            ScrollView {
                id: globalScrollView
                width: 480
                anchors.left: homeScrollView.right
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0

                ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ScrollBar.vertical.policy: ScrollBar.AlwaysOn

                Layout.fillHeight: true
                //anchors.left: parent

                Rectangle {
                    color: "blue"
                    anchors.fill: parent
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:1;anchors_height:100;anchors_width:100;anchors_x:167;anchors_y:234}D{i:3;anchors_x:175}
D{i:2;anchors_height:500}
}
##^##*/
